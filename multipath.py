# importing Python libraries

import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as sst

def ecdf(data, normalize=-999.0):
    '''
    generates CDF from empirical data 
    data - empirical data as a numpy vector
    normalize - percentil (%) to set zero (no normalization if outside <0,100>)
    returns - (x, y) vectors in tuple    
    '''
    x = np.sort(data)
    if 0 <= normalize <= 100.0: 
        x -= np.percentile(data, normalize)
    y = np.arange(1, len(x)+1)/float(len(x))
    return (x, y)

# II - Small scale effects
raw_data_II_rx = ['./Zmerene_data/RecTrace_021.csv','./Zmerene_data/RecTrace_022.csv','./Zmerene_data/RecTrace_023.csv'] # data files representing measurements for fixed antennas
raw_data_II_t = [50, 45,45] # duration in seconds for each file
raw_data_II_name = ["Žádné zastínění", "Dynamické zastínění", "Maximální zastínění"]
# Reading raw measurement data 
fig, axs = plt.subplots(1, len(raw_data_II_rx), figsize=(20, 5))
Rx = []
t = []
for i, f in enumerate(raw_data_II_rx):
    Rx.append([])
    with open(f) as ff:
        for line in ff.read().split(): # each line = 1601 readings from the PR100 receiver screen                    
            Rx[i].append(max([float(x) for x in line.split(';')])) # extracting RX value from raw data as a maximum value of each sweep screen     
    print(raw_data_II_name[i])
    print("median",np.median(Rx[i]))
    print("minimum",min(Rx[i]))
    print("maximum",max(Rx[i]))
    Rx[i] = Rx[i] - np.median(Rx[i]) # normalize to median
    
    
    t.append(np.arange(0, raw_data_II_t[i], raw_data_II_t[i]/len(Rx[i])))
    
    axs[i].set_ylim(-110,-45)
    axs[i].plot(t[i], Rx[i])
    axs[i].set(xlabel='čas (s)', ylabel='normalizovaná hodnota RSS k mediánu (dB)', title=raw_data_II_name[i])
    #axs[i].set(xlabel='čas (s)', ylabel='hodnota RSS (dB)', title=raw_data_II_name[i])
    axs[i].grid()
#plt.rcParams.update({'font.size': 30})
plt.show()



# Show empirical CDF and compare with Rayleigh distribution
plt.figure(figsize=(9, 7))
for i, rx in enumerate(Rx):
    xcdf, ycdf = ecdf(rx, 50)
    plt.semilogy(xcdf, 100*ycdf, label=raw_data_II_name[i])
xr = np.arange(np.power(10.0, -30/20), np.power(10.0, 10/20), 0.1)
yr = sst.rayleigh.cdf(xr)
xr = 20.0*np.log10(xr/sst.rayleigh.median())
plt.semilogy(xr,100*yr, label='Rayleigh')
plt.plot([0,0],[0.01,100], label='nulový úbytek')
plt.plot([-30,10],[50,50], label='50%')
plt.ylim(0.1,100)
plt.xlim(-30,10)
plt.xlabel('Relativní úbytek (dB), normalizovaný (0 dB na 50%)')
plt.ylabel('Kumulativní pravděpodobnost (%)')
plt.title('CDF')
plt.legend()
plt.grid()
plt.show()
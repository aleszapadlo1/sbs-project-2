

# importing Python libraries
#%matplotlib inline
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as sst


def LOSFading(traceName, d_start, d_end):
    # Reading raw measurement data 
    rRX = [];
    ff = open(traceName);
    rRX = ff.read().split() # each line = 1601 readings from the R&S PR100 receiver screen
    print(len(rRX), 'readings')
    if d_start > d_end: # reverse the data so the distance would be encreasing
        reverse_ = [rRX[len(rRX) - i] #brute force reverse, the provided function did not work
            for i in range(1, len(rRX)+1)]
        rRX = reverse_
        d_start, d_end = d_end, d_start
    screens_dBm = []
    for line in rRX:
        screens_dBm.append( [float(x) for x in line.split(';')]   )
    Rx = [max(x) for x in screens_dBm]
    return Rx

def pathLoss(data,d_start, d_end, F_kons):
    d = np.array(range(len(data)))
    d = d_start + d * (d_end-d_start)/len(d)
    L = data[0] - np.array(data) + F_kons
    return d,L

def movingAvg(data,window):
    win=15 # slide window shoud consider the wavelength and environment
    Lf=[]
    for i in range(len(data)):
        imin = max(0,i-window)
        imax = min(len(data),i+window)
        Lf.append(sum(data[j] for j in range(imin, imax))/(imax-imin))
    return Lf

def oneSlopePrediction(data, distance, fresnelLine, fresnelIndex):

   


    data_pre = data[:fresnelIndex]
    data_post = data[fresnelIndex:]
    distance_pre = distance[:fresnelIndex]
    distance_post = distance[fresnelIndex:]

    #path loss modeling
    logd_pre = np.log10(distance_pre)
    n_pre, L1_pre, rv, pv, stderr = sst.linregress(logd_pre, data_pre) # fit the model
    Lp_pre = L1_pre + n_pre * logd_pre # prediction by the derived model

    logd_post = np.log10(distance_post)
    n_post, L1_post, rv, pv, stderr = sst.linregress(logd_post, data_post) # fit the model
    Lp_post = L1_post + n_post * logd_post # prediction by the derived model
    # show plot
    plt.figure(figsize=(9, 5))
    plt.semilogx(distance, data, '+', lw=0.1, label='naměřená data')
    plt.semilogx(distance_pre, Lp_pre, lw=3, label=f'Pre-Fresnel 1SM: L1 = {L1_pre:.1f} dB, n = {0.1*n_pre:.1f}', color = "green")
    plt.semilogx(distance_post, Lp_post, lw=3, label=f'Post-Fresnel 1SM: L1 = {L1_post:.1f} dB, n = {0.1*n_post:.1f}', color = "red")
    plt.axvline(x=fresnelLine,color = "orange",label = "Fresnelův zlom") 
    plt.xlabel('Vzdálenost antén (m)')
    plt.ylabel('Útlum (dB)')
    plt.title('Predikovaný útlum 1SM vs. naměřená data')
    plt.legend()
    plt.grid()


    # calculate prediction error and print results
    err_pre = Lp_pre-data_pre
    stdev_pre = np.std(err_pre)
    print('Pre-Fresnel : Empirical One Slope Model parameters:')
    print('L1 =', L1_pre.round(1), 'dB')
    print('n =', (0.1*n_pre).round(1))
    print('standard deviation =', stdev_pre.round(1), 'dB')
    print('mean error =', np.mean(np.abs(err_pre)).round(1), 'dB')

    #calculate prediction error and print results
    err_post = Lp_post-data_post
    stdev_post = np.std(err_post)
    print('Post-Fresnel : Empirical One Slope Model parameters:')
    print('L1 =', L1_post.round(1), 'dB')
    print('n =', (0.1*n_post).round(1))
    print('standard deviation =', stdev_post.round(1), 'dB')
    print('mean error =', np.mean(np.abs(err_post)).round(1), 'dB')

    

   

    return (err_pre,stdev_pre,Lp_pre,err_post,stdev_post,Lp_post)

def pdf(err,stdev,title):
    plt.figure()
    plt.hist(err, bins=40, density=True, label='Empirické PDF')
    # comparison with Log-Normal distribution
    pdfx = np.linspace(-30,30)
    ppdf = sst.norm.pdf(pdfx, scale=stdev)
    plt.plot(pdfx, ppdf, label='Fit s normální distribucí')
    plt.title(title)
    plt.legend()
    plt.grid()

def ecdf(data,stdev,title,normalize=50.0):
    ''' 
    generates CDF from empirical data 
    data - empirical data as a numpy vector
    normalize - percentil (%) to set zero (no normalization if outside <0,100>)
    returns - (x, y) vectors in tuple    
    '''
    x = np.sort(data)
    if 0 <= normalize <= 100.0: 
        x -= np.percentile(data, normalize)
    y = np.arange(1, len(x)+1)/float(len(x))

    plt.figure(figsize=(9, 7))
    plt.semilogy(x, 100*y, label='Empirické CDF')
    plt.semilogy(x,100*sst.norm.cdf(x,scale=stdev), label='Normal CDF fit')
    plt.ylim(0.1,100)
    plt.xlabel('Normalizované predikované RSS (dB)')
    plt.ylabel('Procenta lokací s RSS < predikované RSS (%)')
    plt.title(title)
    plt.legend()
    plt.grid()

def predictingRSS(Loss, Loss_predicted ,distance, fresnelLine, fresneIndex, stdev_pre, stdev_post):
    Pn = 20

    percent90ConfidenceInterval_pre = stdev_pre*1.64;
    percent90ConfidenceInterval_post = stdev_post*1.64;
    print('90% confidence interval pre-Fresnel +- [dB]:',percent90ConfidenceInterval_pre )
    print('90% confidence interval post-Fresnel +- [dB]:',percent90ConfidenceInterval_post )

    plt.figure(figsize=(9, 9))
    plt.subplot(211)
    plt.semilogx(distance, Pn-Loss, '+')
    plt.plot(distance, Pn-Loss_predicted, lw =3, label='50 % míst překočí tuto hodnotu', color = "orange")

    plt.plot(distance[:fresneIndex], Pn-Loss_predicted[:fresneIndex]-percent90ConfidenceInterval_pre, lw =3, color = "green")
    plt.plot(distance[:fresneIndex], Pn-Loss_predicted[:fresneIndex]+percent90ConfidenceInterval_pre, lw =3, color = "red")

    plt.plot(distance[fresneIndex:], Pn-Loss_predicted[fresneIndex:]-percent90ConfidenceInterval_post, lw =3, label='90 % míst překočí tuto hodnotu', color = "green")
    plt.plot(distance[fresneIndex:], Pn-Loss_predicted[fresneIndex:]+percent90ConfidenceInterval_post, lw =3, label='10 % míst překočí tuto hodnotu', color = "red")


    plt.axvline(x=fresnelLine,color = "orange",label = "Fresnelův zlom") 
    plt.xlabel('Vzdálenost (m)')
    plt.ylabel('RSS (dBm)')
    plt.title('Predikované RSS (Vzdálenost v log. i lineárním měřítku')
    plt.legend()
    plt.grid()
    plt.subplot(212)

    plt.plot(distance, Pn-Loss, '+')
    plt.plot(distance, Pn-Loss_predicted, lw =3, label='50 % míst překočí tuto hodnotu',color = "orange" )
    plt.plot(distance[:fresneIndex], Pn-Loss_predicted[:fresneIndex]-percent90ConfidenceInterval_pre, lw =3, color = "green")
    plt.plot(distance[:fresneIndex], Pn-Loss_predicted[:fresneIndex]+percent90ConfidenceInterval_pre, lw =3, color = "red")

    plt.plot(distance[fresneIndex:], Pn-Loss_predicted[fresneIndex:]-percent90ConfidenceInterval_post, lw =3, label='90 % míst překočí tuto hodnotu', color = "green")
    plt.plot(distance[fresneIndex:], Pn-Loss_predicted[fresneIndex:]+percent90ConfidenceInterval_post, lw =3, label='10 % míst překočí tuto hodnotu', color = "red")
    plt.axvline(x=fresnelLine,color = "orange",label = "Fresnelův zlom") 
    plt.xlabel('Vzdálenost (m)')
    plt.ylabel('RSS (dBm)')
    plt.grid()
# INPUT
# I - Large scale effects / Empirical model
#raw_data_I_rx =  ['./RecTrace_010.csv','./RecTrace_011.csv'] # data representing equidistant measurement along a straight path
#d_start = 10 # path starting point - antennas distance (m)
#d_end = 220 # poth end-point - antennas distance (m)
#Fkonst = 58 # a value from the power balance used below to extract the loss (dB)

# II - Small scale effects
#raw_data_II_rx = ['./RecTrace_021.csv','./RecTrace_022.csv'] # data files representing measurements for fixed antennas
#raw_data_II_t = [45, 50] # duration in seconds for each file
F_kons = 45; #dB

#data visualization
openSpaceTo = LOSFading('./Zmerene_data/RecTrace_010.csv',10,220);
openSpaceBack = LOSFading('./Zmerene_data/RecTrace_011.csv',220,10);

lengthDifference = len(openSpaceBack)-len(openSpaceTo);
openSpaceBackTrimmed = openSpaceBack[:len(openSpaceBack)-lengthDifference]

removeFirstElements = 80
openSpaceToTrimmed = openSpaceTo[removeFirstElements:]
openSpaceBackTrimmed = openSpaceBackTrimmed[removeFirstElements:]


#data average of the two measurements
avgRSS = []
for index in range(len(openSpaceToTrimmed)):
    avgRSS.append( (openSpaceToTrimmed[index] + openSpaceBackTrimmed[index])/2 )

smoothRSS=movingAvg(avgRSS, 15);

#distance,pathLossTo = pathLoss(openSpaceToTrimmed,16,220,F_kons)
#distance,pathLossBack = pathLoss(openSpaceBackTrimmed,220,16,F_kons)
distance,pathLoss = pathLoss(avgRSS,16,220,F_kons)

plt.figure(0)
plt.plot(distance,openSpaceToTrimmed, color = "blue", label = "Měření 1")
plt.plot(distance,openSpaceBackTrimmed, color = "orange", label = "Měření 2")
plt.plot(distance,avgRSS,lw=2,alpha=0.8, color = "green", label = "Průměrná hodnota")
plt.plot(distance,smoothRSS,lw=2,alpha=0.8, color = "black",label = "Klouzavý průměr")
plt.legend()
plt.title('RSS (dBm)')
plt.xlabel('Vzdálenost (m)')
plt.ylabel('max Rx výkon (dBm)')
plt.grid()

smoothPathLoss=movingAvg(pathLoss, 15);

anth1 = 1.5
anth2 = 1.5
wavelength = 0.136 #meters
print("ant h1=",anth1," , Ant h2=",anth2," , wavelength=",wavelength)

#fresnel line calculation
fresnelLine = 4*anth1*anth2/wavelength;
print("Fresnel line [m]: ", fresnelLine)

plt.figure(1)
#plt.plot(distance,pathLossTo)
#plt.plot(distance,pathLossBack)
plt.plot(distance,pathLoss,label = "Naměřený průměrný útlum")
plt.plot(distance,smoothPathLoss,color = "black",label = "Klouzavý průměr")
plt.axvline(x=fresnelLine,color = "orange",label = "Fresnelův zlom")
plt.legend()
plt.title('Path loss (dBm)')
plt.xlabel('distance (m)')
plt.ylabel('Path loss (dB)')
plt.grid()

 #najdeme index na ose x který odpovídá vzdálenosti fresnelova spoje
fresnelIndex = 0
for i in range(0,len(distance)-1):
    if fresnelLine >= distance[i] and fresnelLine < distance[i+1]:
        if abs(distance[i]-fresnelLine) < abs(distance[i+1]-fresnelLine):
            fresnelIndex = i
        else:
            fresnelIndex = i+1
        break

err,stdev,Lp,err_post,stdev_post,Lp_post = oneSlopePrediction(pathLoss,distance, fresnelLine, fresnelIndex);
#errBack,stdevBack,LpredictedBack = oneSlopePrediction(pathLossBack,distanceBack);

pdf(err,stdev,'Naměřené PDF vs. modelové PDF před Fresnelovým zlomem')
ecdf(err, stdev,'Naměřené CDF vs. modelové CDF před Fresnelovým zlomem', 50)

pdf(err_post,stdev_post,'Naměřené PDF vs. modelové PDF po Fresnelovým zlomem')
ecdf(err_post, stdev_post,'Naměřené CDF vs. modelové CDF po Fresnelově zlomu', 50)

a = np.array(Lp)
b = np.array(Lp_post)
predictedLoss = np.concatenate([a, b])

predictingRSS(pathLoss,predictedLoss,distance,fresnelLine, fresnelIndex, stdev, stdev_post)

plt.show()

